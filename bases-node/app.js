const { createFile } = require("./utils/utils");
const argv = require("./config/yargs");
require("colors");

createFile(argv.b, argv.l, argv.u)
  .then((nameFile) => console.log("Create :".green, nameFile.rainbow))
  .catch((err) => console.log(err));
