const fs = require("fs");

const createFile = async (base = 5, list = false, until = 10) => {
  try {
    let data = "";
    const fileName = `tabla-${base}.txt`;
    const path = `./data/${fileName}`;

    for (i = 1; i <= until; i++) {
      data += `${base} X ${i} = ${base * i}\n`;
    }

    fs.writeFileSync(path, data);

    if (list) {
      console.log(data);
    }
    return fileName;
  } catch (err) {
    throw err;
  }
};

module.exports = {
  createFile,
};
